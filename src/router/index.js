import Vue from "vue";
import VueRouter from "vue-router";
import Index from "../views/Index.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/favorite/:uid",
    name: "favorite",
    component: () => import(/* webpackChunkName: "favorite" */ "../views/Favorite.vue"),
    props: true,
  },
  {
    path: "/cart/:uid",
    name: "cart",
    component: () => import(/* webpackChunkName: "cart" */ "../views/Cart.vue"),
    props: true,
  },
  {
    path: "/product",
    name: "AllProduct",
    meta: {
      keepAlive: true,
    },
    component: () => import(/* webpackChunkName: "allproduct" */ "../views/AllProduct.vue"),
  },
  {
    path: "/details/:pid",
    name: "Details",
    component: () => import(/* webpackChunkName: "details" */ "../views/Details.vue"),
    props: true,
  },
  {
    path: "/about",
    name: "About",
    component: () => import(/* webpackChunkName: "about" */ "../views/About.vue"),
  },
  {
    path: "/",
    component: Index,
    name: "index",
    meta: {
      keepAlive: true,
    },
  },
];

const router = new VueRouter({
  mode: "hash",
  base: process.env.BASE_URL,
  routes,
});

export default router;
