import request from "@/utils/request.js";

// 获取轮播图
export function getCarousel(params) {
  return request({
    url: "/car",
    method: "get",
  });
}

// 获取首页商品
export function getHomeProduct(params) {
  return request({
    url: "/product",
    method: "get",
  });
}

// 获取分类商品
export function getClassProduct(params) {
  return request({
    url: `/getclass?id=${params.id}`,
  });
}

// 获取商品轮播图图片
export function getDetailsImg(params) {
  return request({
    url: `/details?pid=${params.pid}`,
    method: "get",
  });
}

// 加入购物车
export function addCart(params) {
  return request({
    url: `/addcart?uid=${params.uid}&pid=${params.pid}`,
    method: "get",
  });
}

// 加入收藏夹
export function addFavorite(params) {
  return request({
    url: `/addfavorite?uid=${params.uid}&pid=${params.pid}`,
    method: "get",
  });
}

export function login(params) {
  return request({
    url: `/login?uname=${params.uname}&upwd=${params.upwd}`,
    method: "get",
  });
}

// 修改购物车商品件数
export function updateCartNum(params) {
  return request({
    url: `/upd?cid=${params.cid}&count=${params.count}`,
    method: "get",
  });
}

// 获取购物车的内容
export function getCart(params) {
  return request({
    url: `/setcart?uid=${params.uid}`,
    method: "get",
  });
}

// 删除购物车商品
export function delProduct(params) {
  return request({
    url: `/del?cid=${params.cid}`,
    method: "get",
  });
}

// 删除收藏夹商品
export function deleteFavoriteProduct(params) {
  return request({
    url: `/delfavorite?fid=${params.fid}`,
    method: "get",
  });
}

// 获取收藏夹内容
export function getFavoriteProduct(params) {
  return request({
    url: `/getfavorite?uid=${params.uid}`,
    method: "get",
  });
}
