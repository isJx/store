import axios from "axios";
import store from "../store";
import router from "../router";
import { Message } from "element-ui";

// 创建axios实例
const service = axios.create({
  baseURL: "http://localhost:5000/",
  timeout: 50000, //请求超时时间
});

const successCode = [200, 405, 603]; //不报错的code
// response 拦截器,数据返回后进行一些处理
service.interceptors.response.use(
  (response) => {
    const res = response.data;
    const str = response.config.url.split("?")[0];
    if (localStorage.getItem("user") == null && (str == "/getfavorite" || str == "/setcart")) {
      router.push("/");
      Message({
        message: "登录已过期，请重新登录！",
        type: "error",
      });
      return;
    } else {
      if (successCode.indexOf(res.code) != -1) {
        return res;
      } else if (res.code == "401" || res.code == "407") {
        // 提示用户,然后跳转到登陆页面
        Message("请先登录");
        store.dispatch("LogOut", store.getters.userInfo).then(() => {
          router.push({
            name: "/",
          });
        });
        return res;
      } else {
        return Promise.reject(res);
      }
    }
  },
  (error) => {
    console.log(error);
    if (!error.__CANCEL__) {
      return Promise.reject("网络异常");
    }
  }
);
export default service;
